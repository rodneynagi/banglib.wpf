﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace BangLib.Wpf {

    public class DataGridHelper {

        public static readonly DependencyProperty AttachProperty = DependencyProperty.RegisterAttached(
            "Attach",
            typeof(bool),
            typeof(DataGridHelper),
            new PropertyMetadata(false, OnAttachChanged)
        );

        public static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.RegisterAttached(
            "SelectedItems",
            typeof(ObservableCollection<object>),
            typeof(DataGridHelper),
            new FrameworkPropertyMetadata(default(IList), OnSelectedItemsChanged)
        );

        private static readonly DependencyProperty IsUpdatingProperty = DependencyProperty.RegisterAttached(
            "IsUpdating",
            typeof(bool),
            typeof(DataGridHelper)
        );

        public static bool GetAttach(DependencyObject dataGrid) {
            return (bool)dataGrid.GetValue(AttachProperty);
        }

        public static void SetAttach(DependencyObject dataGrid, bool value) {
            dataGrid.SetValue(AttachProperty, value);
        }

        public static ObservableCollection<object> GetSelectedItems(DependencyObject dataGrid) {
            return dataGrid.GetValue(SelectedItemsProperty) as ObservableCollection<object>;
        }

        public static void SetSelectedItems(DataGrid dataGrid, ObservableCollection<object> selectedItems) {
            dataGrid.SetValue(SelectedItemsProperty, selectedItems);
        }

        private static void SetIsUpdating(DataGrid dataGrid, bool value) {
            dataGrid.SetValue(IsUpdatingProperty, value);
        }

        private static bool GetIsUpdating(DataGrid dataGrid) {
            return (bool)dataGrid.GetValue(IsUpdatingProperty);
        }

        private static void OnAttachChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            if (!(sender is DataGrid dataGrid))
                return;

            if ((bool)e.OldValue)
                dataGrid.SelectionChanged -= SelectionChanged;

            if ((bool)e.NewValue)
                dataGrid.SelectionChanged += SelectionChanged;
        }

        private static void OnSelectedItemsChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            DataGrid dataGrid = sender as DataGrid;
            dataGrid.SelectionChanged -= SelectionChanged;
            if (!GetIsUpdating(dataGrid))
                throw new NotImplementedException("SelectedItems is OneWayToSource only for now!");
            dataGrid.SelectionChanged += SelectionChanged;
        }

        private static void SelectionChanged(object sender, SelectionChangedEventArgs e) {
            DataGrid dataGrid = sender as DataGrid;
            SetIsUpdating(dataGrid, true);
            ObservableCollection<object> items = new ObservableCollection<object>();
            foreach (object item in dataGrid.SelectedItems)
                items.Add(item);
            SetSelectedItems(dataGrid, items);
            SetIsUpdating(dataGrid, false);
        }
    }
}