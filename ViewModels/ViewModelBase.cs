﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BangLib.Wpf.ViewModels {

    public abstract class ViewModelBase : INotifyPropertyChanged {

        public virtual event PropertyChangedEventHandler PropertyChanged;

        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null) {
            if (Equals(storage, value)) {
                return false;
            };

            storage = value;
            OnPropertyChanged(propertyName);

            return true;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public abstract class ViewModelBase<T> : ViewModelBase {
        private T _Data;

        public virtual T Data {
            get {
                return _Data;
            }
            set {
                SetProperty(ref _Data, value);
            }
        }

        public ViewModelBase(T data) {
            Data = data;
        }
    }
}