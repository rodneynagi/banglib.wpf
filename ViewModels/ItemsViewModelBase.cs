﻿using System.Collections.ObjectModel;

namespace BangLib.Wpf.ViewModels {

    public abstract class ItemsViewModelBase<T> : ViewModelBase {
        private ObservableCollection<T> _Items;

        public ObservableCollection<T> Items {
            get {
                if (_Items == null)
                    _Items = new ObservableCollection<T>();
                return _Items;
            }
            set {
                SetProperty(ref _Items, value);
            }
        }
    }

    public abstract class SelectableItemsViewModelBase<T> : ItemsViewModelBase<T> {
        private T _SelectedItem;

        private ObservableCollection<T> _SelectedItems;

        public T SelectedItem {
            get {
                return _SelectedItem;
            }
            set {
                SetProperty(ref _SelectedItem, value);
            }
        }

        public ObservableCollection<T> SelectedItems {
            get {
                if (_SelectedItems == null)
                    _SelectedItems = new ObservableCollection<T>();
                return _SelectedItems;
            }
            set {
                SetProperty(ref _SelectedItems, value);
            }
        }
    }
}