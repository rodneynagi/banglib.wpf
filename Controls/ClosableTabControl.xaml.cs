﻿using BangLib.Wpf.ViewModels;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace BangLib.Wpf.Controls {

    public partial class ClosableTabControl : UserControl {

        private static readonly DependencyProperty OpenTabsProperty = DependencyProperty.Register(
            nameof(OpenTabs),
            typeof(ObservableCollection<ClosableTabItemBinding>),
            typeof(ClosableTabControl),
            new PropertyMetadata(new ObservableCollection<ClosableTabItemBinding>())
        );

        public ClosableTabControl() {
            InitializeComponent();
        }

        public ObservableCollection<ClosableTabItemBinding> OpenTabs {
            get {
                return (ObservableCollection<ClosableTabItemBinding>)GetValue(OpenTabsProperty);
            }
            set {
                SetValue(OpenTabsProperty, value);
            }
        }

        private void CloseTabByGuid(string guid) {
            ClosableTabItemBinding tab = OpenTabs.Where(x => x.ClosingGuid.ToString() == guid).FirstOrDefault();
            if (tab != null && tab.Closable) {
                OpenTabs.Remove(tab);
            }
        }

        public void CloseAllClosableTabs() {
            foreach (ClosableTabItemBinding tab in OpenTabs.Where(x => x.Closable).ToList())
                OpenTabs.Remove(tab);
        }

        public void AddOrRefreshTab(string id, string header, FrameworkElement content, bool canClose = true, Brush background = null, bool focusOnAdd = true) {
            ClosableTabItemBinding oldTab = OpenTabs.FirstOrDefault(x => x.Id == id);
            if (oldTab != null) {
                oldTab.Header = header;
                oldTab.Content = content;
                oldTab.Closable = canClose;
                oldTab.Background = background;
                ThisTabControl.SelectedIndex = OpenTabs.IndexOf(oldTab);
            } else {
                OpenTabs.Add(new ClosableTabItemBinding() {
                    Id = id,
                    Header = header,
                    Content = content,
                    Closable = canClose,
                    Background = background
                });
                if (focusOnAdd)
                    ThisTabControl.SelectedIndex = ThisTabControl.Items.Count - 1;
            }
        }

        private void CloseTab_Click(object sender, RoutedEventArgs e) {
            CloseTabByGuid(((Button)sender).Tag.ToString());
        }

        private void TabHeader_MouseUp(object sender, MouseButtonEventArgs e) {
            if (e.ChangedButton == MouseButton.Middle && e.MiddleButton == MouseButtonState.Released) {
                CloseTabByGuid(((FrameworkElement)sender).Tag.ToString());
            }
        }
    }
}