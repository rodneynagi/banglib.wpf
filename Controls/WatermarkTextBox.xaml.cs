﻿using System.Windows;
using System.Windows.Controls;

namespace BangLib.Wpf.Controls {

    public partial class WatermarkTextBox : UserControl {

        private static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            nameof(Text),
            typeof(string),
            typeof(WatermarkTextBox),
            new PropertyMetadata(string.Empty)
        );

        private static readonly DependencyProperty WatermarkTextProperty = DependencyProperty.Register(
            nameof(WatermarkText),
            typeof(string),
            typeof(WatermarkTextBox),
            new PropertyMetadata(string.Empty)
        );

        private static readonly DependencyProperty MaxLengthProperty = DependencyProperty.Register(
            nameof(MaxLength),
            typeof(int),
            typeof(WatermarkTextBox),
            new PropertyMetadata(0)
        );

        private static readonly DependencyProperty TextWrappingProperty = DependencyProperty.Register(
            nameof(TextWrapping),
            typeof(TextWrapping),
            typeof(WatermarkTextBox),
            new PropertyMetadata(TextWrapping.NoWrap)
        );

        public WatermarkTextBox() {
            InitializeComponent();
        }

        public string Text {
            get {
                return (string)GetValue(TextProperty);
            }
            set {
                SetValue(TextProperty, value);
            }
        }

        public string WatermarkText {
            get { return (string)GetValue(WatermarkTextProperty); }
            set { SetValue(WatermarkTextProperty, value); }
        }

        public int MaxLength {
            get {
                return (int)GetValue(MaxLengthProperty);
            }
            set {
                SetValue(MaxLengthProperty, value);
            }
        }

        public TextWrapping TextWrapping {
            get {
                return (TextWrapping)GetValue(TextWrappingProperty);
            }
            set {
                SetValue(TextWrappingProperty, value);
            }
        }
    }
}