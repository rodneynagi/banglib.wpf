﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace BangLib.Wpf.Converter {

    public class NullToTypeConverter : IValueConverter {
        
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            bool result = true;
            if (value == null ||
                value.GetType() == typeof(bool) && !(bool)value ||
                value.GetType() == typeof(string) && (string.IsNullOrEmpty(value.ToString()) || string.IsNullOrWhiteSpace(value.ToString())) ||
                (value.GetType() == typeof(int) || value.GetType().IsEnum) && (int)value <= 0 ||
                typeof(IEnumerable<object>).IsAssignableFrom(value.GetType()) && ((ICollection)value).Count == 0 ||
                typeof(IList<object>).IsAssignableFrom(value.GetType()) && ((IList)value).Count == 0)
                result = false;
            if (parameter != null && parameter.ToString() == true.ToString())
                result = !result;
            if (targetType == typeof(bool))
                return result;
            else if (targetType == typeof(Visibility))
                return result ? Visibility.Visible : Visibility.Collapsed;
            else
                throw new NotImplementedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}